== Changelog ==

v1.0.0.2
* assembly version  is added
* log and changelog included to Project.
* GroupController add overloaded action GetClientGroupUser.

v1.0.0.3
* Code Sync.
* Enhance Loginng.

v1.4.0.0
* New Controller Commands is added with GetObjectCommands and GetSendSMSCommand Methods.
* Capture Exception Loging in All Controllers.

v1.5.0.0
* Add Trip Method to get Data to from tavlService
* Add reverse geocoding w.r.t to client or user reverseGeocoding source

v1.7.0.1
* Filtering clients and groups depending on MasterGroups.

v1.7.0.2
* Validation of requested group accessibility.

v1.7.1.0
* Added alert history feature.